:: --------------------------------------------------------------------------
:: CMD: Auto-open PuTTY sessions for docker containers
:: @author Hervé Perchec <herve.perchec@gmail.com>
:: --------------------------------------------------------------------------

:: Disable output excepted 'echo' messages and error messages
@echo off

:: Enable UTF-8 output message (redirect output to NUL to not show message)
chcp 65001 > NUL

:: Enable color output
for /F %%a in ('echo prompt $E ^| cmd') do (
  set "ESC=%%a"
)

:: Show script header
echo %ESC%[36m---------------------------------------------------------------%ESC%[0m
echo %ESC%[36mCMD: Auto-open PuTTY sessions for docker containers%ESC%[0m
echo %ESC%[36mAUTHOR: Hervé Perchec ^<herve.perchec@gmail.com^>%ESC%[0m
echo %ESC%[36m---------------------------------------------------------------%ESC%[0m
echo.

:: --------------------------------------------------------------------------
:: Step 1: Start Database session
:: --------------------------------------------------------------------------

start "Load PuTTY Session: App/Docker/Database" putty.exe -load "App/Docker/Database"

:: --------------------------------------------------------------------------
:: Step 2: Start Server session
:: --------------------------------------------------------------------------

start "Load PuTTY Session: App/Docker/Server" putty.exe -load "App/Docker/Server"

:: --------------------------------------------------------------------------
:: Step 3: Start UI session
:: --------------------------------------------------------------------------

start "Load PuTTY Session: App/Docker/UI" putty.exe -load "App/Docker/UI"

:: --------------------------------------------------------------------------
:: END
:: --------------------------------------------------------------------------

echo %ESC%[32mSSH connections successfully etablished ✔%ESC%[0m
echo.